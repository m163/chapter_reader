# chapter_reader IDEA 在线&本地阅读插件


# 公告信息：

- **最新公告：2.1.7.1 WebStorm有问题的道友，等待审核完成及时更新，或者先加QQ群：251897630下载**
- **历史公告：2.1.6 / 2.1.6.2 版本右侧【Read】窗口无法打开的，请等待2.1.6.3审核通过**
- **历史公告：2.1.6版本准备发布，QQ群优先上传使用，如果有问题及时联系我。**
- **历史公告：及时更新2.1.5版本，远程阅读问题已经修复**


# 文章中所有用【】括起来，仅为侧重表达的意思，无其它含义。

# 1. 介绍
✅ 历史记录自动保存

✅ 自定义适配想看的网站

✅ 在线章节阅读 - gitee配置文件中已配置的网站

✅ 在线全章节阅读

✅ 本地txt章节阅读

✅ 本地txt全章节阅读

✅ 本地epub章节阅读

✅ 本地txt简易阅读

✅ 本地txt换行符阅读

```
"chapter_reader" 是一款IDEA 在线&本地 小说阅读插件
为了帮助各位道友在资本主义的魔爪下，有一个可以调节情绪且可以畅游知识海洋的环境。
"chapter_reader"应运而生。只希望可以帮助更多的人。
(请勿痴迷，该学习则学习，该放松则放松，让加班？不可能！)
希望大家看到这篇文章可以，点赞评论，让更多人知道即可。

遇到问题及时加QQ群，各位道友都会热心帮助的。
插件：chapter_reader
作者：wind
邮件：wind_develop@163.com
QQ群：1群：252192636，2群：251897630（有疑问会有各位道友或作者在线教学）

gitee：https://gitee.com/wind_invade/chapter_reader
博客园：https://www.cnblogs.com/winddniw/p/15689784.html
git: https://windinvade.github.io/chapter_reader/
```



# 2. 安装方式

```
1. IDEA插件市场直接安装：File | Settings | Plugins | 搜索**chapter_reader**

2. 通过idea插件网下载安装：https://plugins.jetbrains.com/plugin/16544-chapter-reader
```



# 3. 使用说明



## 版本更新说明（推荐及时升级最新版）所有待发布版本，均会在QQ群优先上传，IDEA审核需要2-3个工作日。
- **最新待发布【2.1.7 / 2.1.7.1】优化【Read】窗口体验，2.1.7.1紧急修复WebStorm兼容性问题**
- **插件版本【2.1.6】新增单独窗口全章节阅读，优化其它功能体验。**
- **插件版本 < 【2.1.5】，远程阅读会有问题**
- **插件版本 < 【2.1.3】，对高版本IDEA适配会有问题，请及时升级最新版本**
- **插件版本 < 【2.0.7】，旧版配置文件不再受支持，请及时升级最新版。**

```
- v2.1.7 / 2.1.7.1(new):
    2.1.7.1. 校验遗漏紧急修复
    1. 整章阅读【Read】窗口优化，支持修改颜色及配置自动保存
    2. 远程阅读存在下一页兼容
    3. 行号跳转回归
    4. 修复epub读取bug
- v2.1.6：
    1. 新增整章阅读模式（本地/在线均支持）
    2. 在线有效网站修改为展示框，并支持一键跳转
    3. 在线网站调试，新增复制配置一键解析功能，调试更便捷。
    4. 修改部分菜单显示文字
- v2.1.5: 1. 修复远程配置文件读取问题 2. 修复本地缓存bug
- v2.1.4: 
  1. 新增高版本IDEA适配
  2. 新增远程阅读配置文件读取成功后，缓存到本地。（因gitee误封两次，故加此功能）
  3. 【x.x.x】适用低版本IDEA，【x.x.x-high】适用高版本IDEA
```



## 版本号说明（idea插件市场搜索时，会自动显示匹配当前版本）：

### 【x.x.x 如：2.1.5】版本（低版本IDEA）

![在这里插入图片描述](https://img-blog.csdnimg.cn/258acf99e5cc4e8e8ae705184c8f29c2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

### 【x.x.x-high 如：2.1.5-high】版本（高版本IDEA）

![在这里插入图片描述](https://img-blog.csdnimg.cn/ad6cf0146dd445a6b6170057df639335.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)



## 阅读模式说明：
### 初次使用可以先在【插件设置】将菜单名称设置为中文

### 本地章节阅读模式（支持章节跳转）：

```
输入小说标题对应的正则表达式，例：
【第1238章 xxxxxx标题】正则为：【第.*章.*】
【第一卷 第2章 xxxxxx标题】正则：【第.*章.*】也可以识别，或者【.*第.*章.*】
反正自行找个在线正则调试的网站，很快即可成功。读取后即可通过章节跳转，跳转到任意章节。

注：有些小说的额外说明里，会有【该诗句 引用xxx作者 第x章 xxxx】这样的内容，也会被识别为标题。
所以发现这样的情况，在你填写正则的基础上：加上【^】开头，表示只识别【第】开头xxxxx的内容，可以有效过滤这种情况。
```

### 本地全章节阅读模式（支持章节跳转、支持单独窗口整章阅读）：

```
与本地章节阅读模式操作一致，只是操作在右侧单独的窗口【Read】中。
```

### EPUB阅读模式（支持章节跳转）：

```
阅读epub文件，不多叙述。但是如果发现章节显示有问题，请加群私聊并将文件发给我，有时间会进行调试优化。
```

### 简易小说阅读模式：

```
仅按照【插件设置】 > 【设置单行展示最大字数】的值，进行内容分割。
注：无法进行章节跳转，只能一行行阅读（后期会把【行跳转】也加回来）
```

### 简易文本阅读模式：

```
仅按【文件换行】分行，不受“行字数设置”限制，
注：单行字数过多左下角会展示不全。
```

### 在线阅读模式（支持章节跳转）

**写在前面：阅读输入的URL，是【章节目录URL】也就是展示小说【章节目录列表】页面的URL。**

```
1. 点击插件菜单【正常网站列表】可以得到当前节点配置正常的网站。
2. 选择任意一个网站，去找自己想看的小说。
3. 将【章节目录URL】复制，点击插件菜单【远程阅读模式】输入【章节目录URL】进行阅读。
```

### 全章节在线阅读模式（支持章节跳转、支持单独窗口整章阅读）：

```
与在线阅读模式操作一致，只是操作在右侧单独的窗口【Read】中。
```


## 配置文件说明（[chapter_config-v1.json](https://gitee.com/wind_invade/chapter_reader/blob/master/chapter_config-v1.json)）：

```
注：已有的网站无需重复配置，如果节点有误，在之前的基础上修改！！
！！！不需要重复提交同一个网站！！！如果结构变化请在之前基础上修改！！！
chapterPath、contentPath、nextUrlPath 全站通用，随意在网站上找自己想看的小说。

- checkUrl = "文章目录"    文章目录 URL
- chapterPath = "文章目录节点" 文章目录 节点的full path
- contentPath = "章节内容节点" 章节内容 节点的full path
- nextUrlPath = "下一页按钮节点" 部分网站存在下一页的情况，请配置【下一页】节点的full path。
```



## 快捷键设置方法

```
IDEA | File | Settings | Keymap | Plug-ins | 搜索 Read 自行设置
翻页推荐快捷键，其它自行设置：

- 小键盘"+" "-"(目前最轻松的体验方式)
- 鼠标多功能按键
- 按键 + 鼠标滚轮 (必须是按键+鼠标滚轮, 否则会因优先级的问题, 无法生效)
- 组合快捷键(会很累)
```




## 如出现右下角有弹窗提示，操作步骤

```
File | Settings | Appearance & Behavior | Notifications | 搜索novel  | 将Popup 修改为 No popup
```



## 远程阅读FullPath节点获取教程

**写在前面：该步骤较为繁琐，适配的道友仅仅是无私为其ta道友提供帮助。在此非常感谢各位提交网站适配的道友**

**注：已有的网站无需重复配置，如果节点有误在之前的基础上修改
！！！不需要重复提交同一个网站！！！如果结构变化请在之前基础上修改！！！

  - 新增【网站节点调试】可视化界面，调试成功获取后可一键复制配置。
  - 部分网站存在动态生成dom节点的问题，需要使用“保存网页到本地”，打开保存的文件进行FullPath获取。
  - 看完以下步骤，不会的可以加入QQ群，寻求各位道友帮助。

截图说明：
1. 章节目录节点获取，保存起来备用
![在这里插入图片描述](https://img-blog.csdnimg.cn/cd158ec761cb4a73ab7b5f70b1fdf66c.png)

2. 文章内容节点获取，保存起来备用
![在这里插入图片描述](https://img-blog.csdnimg.cn/48c174eea4d2441a8ec4992d889c7115.png)

3. 打开插件菜单【网站节点调试】填入获取的节点
![在这里插入图片描述](https://img-blog.csdnimg.cn/ff6feeef75fe40558e52b6e8c37d8c62.png)

4. 解析无问题后，一键复制配置。

如果网站已经配置过，但是节点失效，则修改该网站即可

如果没有配置过，则新增此配置

提交到gitee -> chapter_config-v1.json。（审核通过后，即可阅读该网站，有时会不及时，群中@我）

# 4. 截图演示

## 菜单展示

![在这里插入图片描述](https://img-blog.csdnimg.cn/3b92fb7dddb84418ad617c083a25f57f.png)

## 设置菜单
![在这里插入图片描述](https://img-blog.csdnimg.cn/57af510e7be54de9a605defae56988e3.png)

## 左下角展示（需将鼠标焦点触发在IDEA内部窗口中）

![在这里插入图片描述](https://img-blog.csdnimg.cn/a00059a3ef01464daa3b907c08e412c2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

## 弹出框展示（需将鼠标焦点放在文件内）

![在这里插入图片描述](https://img-blog.csdnimg.cn/d0867a94033f4b83b14df7c29dd9f226.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2luZF9zc3Nzc3Nzc3M=,size_20,color_FFFFFF,t_70,g_se,x_16)

## 导航栏展示

![在这里插入图片描述](https://img-blog.csdnimg.cn/a1b1de7e6b14474cab40af34fd403c96.png)

## 全章节阅读（右侧窗口，整章阅读，隐蔽性低）
双击鼠标左键隐藏上方按钮

双击鼠标左键并保持不松，拖动一下鼠标即可显示

翻页需点击，后续版本会新增：左← 右→方向键为快捷键
![在这里插入图片描述](https://img-blog.csdnimg.cn/0c0cb95fe159479e8f8f6e33d369fb3e.png)


# 5.特此声明（侵权免责声明）

```
无任何商业用途，无任何侵权想法。但如发现侵权或其它问题请 及时 且 成功 与作者本人取得联系。
作者本人会在第一时间进行相关内容的删除。

插件：chapter_reader
作者：wind
邮件：wind_develop@163.com
QQ群：252192636（有疑问会有各位道友或作者在线教学）
博客园：https://www.cnblogs.com/winddniw/p/15689784.html
csdn： https://blog.csdn.net/com_study/article/details/115709454（吃相难看，会渐渐放弃）

在这祝大家工作开心^_^
```

# 如果好用，还请赞赏鼓励持续更新。

<div style="display:flex;">
    <img src="https://img-blog.csdnimg.cn/20210713161400215.png" width="49%" height="600px"><img src="https://img-blog.csdnimg.cn/20210713162109319.jpeg"   width="49%" height="450px">
</div>

